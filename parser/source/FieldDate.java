import java.util.regex.*;

class FieldDate extends Field
{
	// yyyy.mm.dd
	static private Pattern pattern = Pattern.compile("^\\d\\d\\d\\d\\.\\d\\d\\.\\d\\d$");

	public FieldDate(String aName)
	{
		super(aName);
	}

	public boolean verifyFormat(String s)
	{
		return pattern.matcher(s).matches();
	}

	public boolean search(String field, String needle)
	{
		return field.equals(needle);
	}

	public String suffix()
	{
		return " Date";
	}
}
