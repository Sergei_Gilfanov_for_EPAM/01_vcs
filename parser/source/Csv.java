import java.util.*;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;


class Csv
{
	private CsvParser parser;
	private List<Field> columns;

	public Csv(Reader reader) throws IOException
	{
		parser = new CsvParser(reader);
		columns = readHeader();
	}

	/*
	 * Чтение заголовка Csv файла
	 */
	public List<Field> readHeader() throws IOException
	{
		List<Field> retval = new ArrayList<Field>();
		int c;
		StringBuilder buffer = new StringBuilder();

		while ( (c = parser.rawRead() ) != CsvParser.END_OF_RECORD ){
			if ( c != CsvParser.END_OF_FIELD ) {
				buffer.append((char)c);
				continue;
			}
			
			String unparsedField = buffer.toString();
			String fieldName;
			Field field = null;
			while (true){
				if ( (fieldName = testSuffixGetName(unparsedField, " String")) != null ) {
					field = new FieldString(fieldName);
					break;
				}

				if ( (fieldName = testSuffixGetName(unparsedField, " Integer")) != null ) {
					field = new FieldInteger(fieldName);
					break;
				}

				if ( (fieldName = testSuffixGetName(unparsedField, " Date")) != null ) {
					field = new FieldDate(fieldName);
					break;
				}

				if ( (fieldName = testSuffixGetName(unparsedField, " Float")) != null ) {
					field = new FieldFloat(fieldName);
					break;
				}

				// TODO ругаемся на неизвестный тип столбца
				field = new FieldString(unparsedField.trim());
				break;
			}
/*
			System.out.println(field);
			System.out.println("-" + field.getName() + "-");
*/
			retval.add(field);
			buffer.setLength(0);
		}
		return retval;
	}

	private String testSuffixGetName(String unparsedField, String suffix)
	{
		String trimmed = unparsedField.trim();
		int suffixLength = suffix.length();
		int fieldLength = trimmed.length();
		if (fieldLength < suffixLength) {
			return null;
		}
		// Если хвост b совпадает с суффиксом, 
		// то возвращаем все, что шло перед ним
		if (! trimmed.endsWith(suffix) ) {
			return null;
		}

		return trimmed.substring(0,fieldLength - suffixLength);
	}

	public void search(Writer writer, String columnName, String needle) throws IOException
	{
		// Ищем колонку, по которой будет происходит поиск
		Field hayColumn = null;
		int i = 0;
		for (Field field: columns) {
			if (field.getName().equals(columnName) ) {
				hayColumn = field;
				break;
			}
			i++;
		}
		if ( hayColumn == null ) {
			System.err.println("Не найдена колонка с именем '" + columnName + "'");
			return;
		}
		// Смотрим, подходит ли формат поисковой строки под тип
		if ( !hayColumn.verifyFormat(needle) ) {
			System.err.println("Неверный тип поискового выражения для строки " + columnName);
			return;
		}

		// Можно начать искать. Но сначала выведем в результат колонку заголовков (ее только что
		// парсили при помощи readHeader, поэтому парсер ее помнит)
		// FIXME: возможно это криво, тк требует того, что после этого парсер не использовался.
		writer.write(parser.parsed());
		parser.cleanParsed();


		List<String> currentRecord = new ArrayList<String>();
		StringBuilder currentField = new StringBuilder();

		boolean done = false;
		int currentRow = 1;
		while(!done) {
			int c = parser.unquoteRead();
			switch (c) {
				case CsvParser.END_OF_INPUT:
					done = true;
					continue;
				case CsvParser.END_OF_RECORD:
					// Технически, конец файла сразу после конца строки
					// создает запись из одного поля нулевой длины.
					// Но это несколько неожиданно на практике,
					// поэтому подобную ситуацию в конце файла просто  игнорируем
					if (currentRecord.size() == 1
						&& currentRecord.get(0).length() == 0
						&& parser.peek() == CsvParser.END_OF_INPUT
					) {
						continue;
					}
					currentRow++;
					// Проверяем, ищем и выводим текущую запись

					// Проверка длины записи
					if (columns.size() != currentRecord.size() ) {
						System.err.println("Неверное количество записей в строке " + currentRow);
						System.err.print("Ожидалось " + columns.size());
						System.err.println(", прочитали " + currentRecord.size());
						return;
					}

					// Соответствие формата записей их типу
					ListIterator<Field> columnIterator = columns.listIterator();
					ListIterator<String> fieldIterator = currentRecord.listIterator();
					while (columnIterator.hasNext()) {
						Field column = columnIterator.next();
						String field = fieldIterator.next();
						if ( !column.verifyFormat(field) ) {
							System.err.println("Неверный формат колонки '" +column.getName() 
												+ "' в строке " + currentRow);
							System.err.println(field + " для формата" + column.suffix());
							return;
						}
					}
					
					// В каком поле ищем
					columnIterator = columns.listIterator();
					fieldIterator = currentRecord.listIterator();
					String hayField = null;
					while (columnIterator.hasNext()) {
						Field column = columnIterator.next();
						String field = fieldIterator.next();
						// За компанию определяем, в каком именно поле будем потом искать.
						if (column == hayColumn) {
							hayField = field;
							break;
						}
					}

					//Поиск выражения
					if (hayColumn.search(hayField, needle) ) {
						writer.write(parser.parsed());
					}
					// И чистим все для нового цикла
					parser.cleanParsed();
					currentRecord.clear();
					continue;
				case CsvParser.END_OF_FIELD:
					currentRecord.add(currentField.toString());
					currentField.setLength(0);
					continue;
				default:
					currentField.append((char)c);
			}
		}
	}
}
