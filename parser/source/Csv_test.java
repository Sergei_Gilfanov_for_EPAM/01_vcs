import java.io.StringReader;
import java.io.IOException;


import java.io.IOException;

public class Csv_test
{
	public static void main(String[] args) throws IOException
	{
		StringReader reader;

		//Поиск в несуществующией колонке
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\"\"");
//		new Csv(reader).search("e", "_");

		//Неверная дата в поиске
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\"\"");
//		new Csv(reader).search("b", " 1234.56.78");

		// Неверное вещественное в поиске
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\"\"");
//		new Csv(reader).search("c", "1,2");

		// Неверное целое в поиске
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\"\"");
//		new Csv(reader).search("a", "1");



		// Слишком много колонок
//		reader = new StringReader(
//			"a Integer ; b Date ; c Float; d String\n"
//			+ "1;1999.01.01;1.1;A;" // 5-я колонка - пустая
//		);
//		new Csv(reader).search("d", "_");


		// Слишком мало колонок
//		reader = new StringReader(
//			"a Integer ; b Date ; c Float; d String\n"
//			+ "1;1999.01.01;1.1"
//		);
//		new Csv(reader).search("d", "_");


		//Неверный формат даты в данных
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999-09-09;1,1;asdf\"\"");
//		new Csv(reader).search("b", "1999.09.09");

		//Неверный формат вещественного числа в данных
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1.1;asdf\"\"");
//		new Csv(reader).search("c", "1,1");

		//Неверный формат целого числа в данных
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"+1;1999.09.09;1.1;asdf\"\"");
//		new Csv(reader).search("a", "1");

		//Поиск даты
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\n"
//								+"2;2999.09.09;2,2;qwer");
//		new Csv(reader).search("b", "1999.09.09");

		//Поиск вещественного числа
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\n"
//								+"2;2999.09.09;2,2;qwer");
//		new Csv(reader).search("c", "2,2");

		// Поиск целого
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\n"
//								+"2;2999.09.09;2,2;qwer");
//		new Csv(reader).search("a", "1");


		// Поиск строки
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\n"
// 								+"2;2999.09.09;2,2;qwer\n");
//		new Csv(reader).search("d", "e");

		// Поиск пустой строки
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;\n"
//								+"2;2999.09.09;2,2;qwer\n");
//		new Csv(reader).search("d", "");
		
                // Поиск пустой строки в конце файла
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\n"
// 								+"2;2999.09.09;2,2;");
//		new Csv(reader).search("d", "");

                // Поиск пустой строки в конце файла (перевод строки в конце последней записи)
//		reader = new StringReader("a Integer ; b Date ; c Float; d String\n"
//								+"1;1999.09.09;1,1;asdf\n"
//								+"2;2999.09.09;2,2;\r\n");
//		new Csv(reader).search("d", "");

                // Поиск пустой строки в файле из одной колонки (вообще нет строк данных)
//		reader = new StringReader("a String");
//		new Csv(reader).search("a", "");
		
                // Поиск пустой строки в файле из одной колонки
                // нет строк данных, но заголовок кончается на разделителем записей
                // (формально - должна найтись одна строка, но это запутывает
//		reader = new StringReader("a String\r\n");
//		new Csv(reader).search("a", "");
		
                // Поиск пустой строки в файле из одной колонки
                // есть явно видимые строки с записями
//		reader = new StringReader("a String\r\n\n");
//		new Csv(reader).search("a", "");
		
	}
}
