class FieldString extends Field
{
	public FieldString(String aName)
	{
		super(aName);
	}

	public boolean verifyFormat(String s)
	{
		return true;
	}
	public boolean search(String field, String needle)
	{
		// Особый случай - поиск пустого поля
		if (needle.length() == 0) {
			return field.length() == 0;
		}

		return field.indexOf(needle) >= 0;
	}
	public String suffix()
	{
		return " String";
	}
}
