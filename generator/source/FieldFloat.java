import java.io.IOException;
import java.io.Writer;

public class FieldFloat extends Field {

	public String titleSuffix()
	{
		return " Float";
	}

	public void writeDataTo(Writer w) throws IOException
	{
		String s = generator.nextFloatAsString();
		w.write(s);
	}
}

