import java.util.Random;
import java.util.Date;
import java.text.SimpleDateFormat;

// Генератор случайных данных различных типов
public class DataGen {
	private static Random random = new Random();

	// Запрет вывода контрольных символов
	// контрольный символ понимается в смысле Character.isISOControl
	// 'A character is considered to be an ISO control character if
	// its code is in the range '\u0000' through '\u001F' or in the
	// range '\u007F' through '\u009F'.'
	public final static int NO_CONTROLS = 0x0001;

	// Дополнение к предыдущему флагу. Использование NO_CONTROLS | WITH_NEWLINES
	// исключает из 'control character' символы перевода строк '\n', '\r'
	public final static int WITH_NEWLINES = 0x0002;

	// Запрет вывода пробелов (только ' ', разннобразные пробельные символы не учитываются).
	public final static int NO_SPACE = 0x0004;

	// Запрет вывода ';'
	public final static int NO_SEMICOLON = 0x0008;

	// Запрет вывода '"'
	public final static int NO_QUOTES = 0x0010;

	// chi - кодировка. тут использутся не как кодировка генерируемых
	// символов, а только для определения допустимости символа. Чтобы
	// потом при выводе в затребованной кодировке не было проблем.
	public DataGen()
	{
	}

	public char nextChar(CharsetInfo charsetInfo)
	{
		return nextChar(charsetInfo);
	}

	public char nextChar(CharsetInfo charsetInfo, int flags)
	{
		char[] chartable = charsetInfo.getValidChars();
		char randomChar;
		// Алгоритм простой и не обеспечивает равномерного распределения.
		// Просто генерируем символы до тех пор, пока не попадется допустимый
		// по флагам.
		while(true) {
			int randomIndex = random.nextInt(chartable.length);
			randomChar = chartable[randomIndex];
			if ( (flags & NO_CONTROLS) != 0 ) {
				boolean isNewline = (randomChar == '\r' || randomChar == '\n');
				if ( (flags & WITH_NEWLINES) == 0 && isNewline ) {
					continue;
				}
				if ( Character.isISOControl(randomChar) && !isNewline) {
					continue;
				}
			}

			if ( (flags & NO_SPACE) != 0 && randomChar == ' ' ) {
				continue;
			}

			if ( (flags & NO_SEMICOLON) != 0 && randomChar == ';' ) {
				continue;
			}

			if ( (flags & NO_QUOTES) != 0 && randomChar == '"' ) {
				continue;
			}
			break;
		}
		return randomChar;
	}

	public String nextIntegerAsString()
	{
		int i = random.nextInt(Integer.MAX_VALUE);
		return Integer.toString(i);
	}

	public String nextFloatAsString()
	{
		int a = random.nextInt(100000);
		int b = random.nextInt(100000);
		return String.format("%d,%d", a, b);
	}

	public String nextDateAsString()
	{
		// Мы хотим корректную дату, но не слишком далекую.
		// Поэтому ограничиваем через Integer.MAX_VALUE
		long timestamp = random.nextInt(Integer.MAX_VALUE) * (long)1000;
		Date date = new Date(timestamp);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
		return formatter.format(date);
	}
}

