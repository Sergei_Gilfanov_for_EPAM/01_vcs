import java.util.*;
import java.io.IOException;
import java.io.Writer;
import java.io.OutputStreamWriter;

public class RandomCsv_test
{
	public static void main(String[] args) throws IOException
	{
		CharsetInfo ci = new CharsetInfo("ASCII");
		Writer w = new OutputStreamWriter(System.out, ci.charset);
		RandomCsv csv = new RandomCsv(ci, 10, 15, 5, false);

		csv.writeTo(w);
		w.close();
		System.out.close();
	}

}
