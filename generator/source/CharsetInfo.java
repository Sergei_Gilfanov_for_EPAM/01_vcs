import java.util.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder; 

public class CharsetInfo {
	Charset charset;
	char[] validChars;

	public CharsetInfo(String charsetName)
	{
		charset = Charset.forName(charsetName);
		validChars = scanForValidChars(charset);
	}

	public Charset getCharset() {
		return charset;
	}

	public char[] getValidChars() {
		return validChars;
	}

	private char[] scanForValidChars(Charset charset)
	{
		StringBuilder buf = new StringBuilder();
		CharsetEncoder encoder = charset.newEncoder();
		for (int i = Character.MIN_VALUE; i < Character.MAX_VALUE; i++) {
			if ( ! encoder.canEncode((char)i)) {
				continue;
			}
			buf.append((char)i);
		}
		return buf.toString().toCharArray();
	}
}



