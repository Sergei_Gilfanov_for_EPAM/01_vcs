import java.io.IOException;
import java.io.Writer;

//Интерфейс для работы с колонками

abstract public class Field {
	static DataGen generator = new DataGen();

	abstract public String titleSuffix();
	abstract public void writeDataTo(Writer w) throws IOException;
}

