import java.util.*;
import java.io.IOException;

public class DataGen_test 
{
	public static void main(String[] args) throws IOException
	{
		CharsetInfo ci = new CharsetInfo("ASCII");
		DataGen g = new DataGen(); 

/*		char[] a1 = new char[10];
		for (int i = 0; i < a1.length; i++) {
			a1[i] = (char)g.nextChar();
		}
		System.out.println(a1);
*/
/*		char[] a2 = new char[10];
		for (int i = 0; i < a2.length; i++) {
			a2[i] = (char)g.nextChar(DataGen.NO_CONTROLS);
		}

		System.out.println(a2);
*/


		char[] a3 = new char[100];
		for (int i = 0; i < a3.length; i++) {
			a3[i] = (char)g.nextChar(ci, DataGen.NO_CONTROLS | DataGen.WITH_NEWLINES | DataGen.NO_SPACE|  DataGen.NO_SEMICOLON);
		}

		System.out.println(a3);
	}
}
