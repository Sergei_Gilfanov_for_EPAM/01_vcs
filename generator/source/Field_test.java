import java.util.*;
import java.io.IOException;
import java.io.Writer;
import java.io.OutputStreamWriter;

public class Field_test
{
	public static void main(String[] args) throws IOException
	{
		CharsetInfo ci = new CharsetInfo("ASCII");
		Writer w = new OutputStreamWriter(System.out, ci.charset);
		Field field;

		field = new FieldString(ci, 50, false);
		field.writeDataTo(w);
		w.write('\n');
/*

		field = new FieldDate();
		field.writeDataTo(w);
		w.write('\n');


		field = new FieldInteger();
		field.writeDataTo(w);
		w.write('\n');

		
		field = new FieldFloat();
		field.writeDataTo(w);
		w.write('\n');
*/
		w.close();
		System.out.close();
	}

}
