import java.io.IOException;
import java.io.Writer;

//Интерфейс для работы с колонками

public class FieldString extends Field {
	CharsetInfo ci;
	int stringMaxLength;
	boolean noeol;
	
	public FieldString(CharsetInfo aCi, int aStringMaxLength, boolean aNoeol)
	{
		ci = aCi;
		stringMaxLength = aStringMaxLength;
		noeol = aNoeol;
	}

	public String titleSuffix()
	{
		return " String";
	}

	private String nextQuotedChar(int flag) {
		char c = generator.nextChar(ci, flag);
		switch (c) {
			case ';':
				return "\";\"";
			case '\n':
				return "\"\n\"";
			case '\r':
				return "\"\r\"";
			case '"':
				return "\"\"";
			default:
				return Character.toString(c);
		}
	}

	public void writeDataTo(Writer w) throws IOException
	{
		int charsLeft = stringMaxLength;
		int flag;
		if ( noeol ) {
			flag = DataGen.NO_CONTROLS;
		} else {
			flag = DataGen.NO_CONTROLS | DataGen.WITH_NEWLINES;
		}

		while (charsLeft > 0) {
			String s = nextQuotedChar(flag);
			// При приближении к ограничению длины мы не можем использовать
			// те символы, что нужно заковычивать, тк после этого мы выйдем
			// за ограничение. Поэтому выбираем тот символ, который подойдет.
			if (s.length() > charsLeft ) {
				continue;
			}
			w.write(s);
			charsLeft -= s.length();
		}
	}
}

