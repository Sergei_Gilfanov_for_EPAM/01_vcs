import java.util.*;
import java.io.IOException;
import java.io.Writer;

//Генератор записей
public class RandomCsv {
	CharsetInfo ci;
	int stringMaxLength;
	int columns;
	int rows;
	boolean noeol;

	private static int COLUMN_NAME_LEN = 10;

	public RandomCsv(CharsetInfo aCi, int aStringMaxLength, int aColumns, int aRows, boolean aNoeol)
	{

		ci = aCi;
		stringMaxLength = aStringMaxLength;
		columns = aColumns;
		rows = aRows;
		noeol = aNoeol;
	}

	private List<Field> generateFieldTypes()
	{
		List<Field> fields = new ArrayList<Field>();
		Random random = new Random();
		int columnsLeft = columns;
		while(columnsLeft-- > 0) {
			Field toAdd;
			switch(random.nextInt(4)) {
				case 0:
					toAdd = new FieldString(ci,stringMaxLength, noeol);
					break;
				case 1:
					toAdd = new FieldDate();
					break;
				case 2:
					toAdd = new FieldInteger();
					break;
				default: // На самом деле может быть только 3
					toAdd = new FieldFloat();
			}
			fields.add(toAdd);
		}
		return fields;
	}

	private void writeFieldTitle(Writer w, Field field) throws IOException
	{
		DataGen generator = new DataGen();
		int flags = DataGen.NO_CONTROLS
			| DataGen.NO_SPACE
			| DataGen.NO_SEMICOLON
			| DataGen.NO_QUOTES;
		for (int charsLeft = COLUMN_NAME_LEN; charsLeft > 0; charsLeft--) {	
			w.write(generator.nextChar(ci, flags));
		}
		String suffix = field.titleSuffix();
		w.write(suffix);
	}

	private void writeHeader(Writer w, List<Field> fieldList) throws IOException
	{
		Field lastField = fieldList.get(fieldList.size() - 1);
		for (Field field: fieldList) {
			writeFieldTitle(w, field);
			if ( field != lastField ) {
				w.write(';');
			}
		}
	}

	private void writeRecord(Writer w, List<Field> fieldList) throws IOException
	{
		Field lastField = fieldList.get(fieldList.size() - 1);
		for (Field field: fieldList) {
			field.writeDataTo(w);
			if ( field != lastField ) {
				w.write(';');
			}
		}
	}

	public void writeTo(Writer w) throws IOException
	{
		List<Field> fieldList = generateFieldTypes();

		//Выводим заголовок
		writeHeader(w, fieldList);
		w.write('\r'); // 0x0D, CR
		w.write('\n'); // 0x0A, LF

		//Выводим данные
		for ( int i = 0; i<rows; i++) {
			writeRecord(w, fieldList);
			w.write('\r'); // 0x0D, CR
			w.write('\n'); // 0x0A, LF
		}
	}
}

