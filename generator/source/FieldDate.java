import java.io.IOException;
import java.io.Writer;	

public class FieldDate extends Field {
	public String titleSuffix()
	{
		return " Date";
	}

	public void writeDataTo(Writer w) throws IOException
	{
		String s = generator.nextDateAsString();
		w.write(s);
	}
}

